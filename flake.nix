{
  description = "A cloud-native LDAP server backed by a Consul datastore";

  inputs.nixpkgs.url = "github:nixos/nixpkgs/0244e143dc943bcf661fdaf581f01eb0f5000fcf";
  inputs.gomod2nix.url = "github:tweag/gomod2nix/40d32f82fc60d66402eb0972e6e368aeab3faf58";

  outputs = { self, nixpkgs, gomod2nix }:
  let
    pkgs = import nixpkgs {
      system = "x86_64-linux";
      overlays = [
        (self: super: {
          gomod = super.callPackage "${gomod2nix}/builder/" { };
        })
      ];
    };
    bottin = pkgs.gomod.buildGoApplication {
      pname = "bottin";
      version = "0.1.0";
      src = builtins.filterSource
        (path: type:  (builtins.match ".*/test/.*\\.(go|sum|mod)" path) == null)
        ./.;
      modules = ./gomod2nix.toml;

      CGO_ENABLED=0;

      meta = with pkgs.lib; {
        description = "A cloud-native LDAP server backed by a Consul datastore";
        homepage = "https://git.deuxfleurs.fr/Deuxfleurs/bottin";
        license = licenses.gpl3Plus;
        platforms = platforms.linux;
      };
    };
  in
  {
    packages.x86_64-linux.bottin = bottin;
    packages.x86_64-linux.default = self.packages.x86_64-linux.bottin;
  };
}
