let
  pkgsSrc = fetchTarball {
    # As of 2022-07-19
    url = "https://github.com/NixOS/nixpkgs/archive/d2db10786f27619d5519b12b03fb10dc8ca95e59.tar.gz";
    sha256 = "0s9gigs3ylnq5b94rfcmxvrmmr3kzhs497gksajf638d5bv7zcl5";
  };
  gomod2nix = fetchGit {
    url = "https://github.com/tweag/gomod2nix.git";
    ref = "master";
    rev = "40d32f82fc60d66402eb0972e6e368aeab3faf58";
  };

  pkgs = import pkgsSrc { 
    overlays = [
      (self: super: {
        gomod = super.callPackage "${gomod2nix}/builder/" { };
      })
    ];
  };
in rec {
  bin = pkgs.gomod.buildGoApplication {
    pname = "bottin-bin";
    version = "0.1.0";
    src = builtins.filterSource 
      (path: type:  (builtins.match ".*/test/.*\\.(go|sum|mod)" path) == null) 
      ./.;
    modules = ./gomod2nix.toml;

    CGO_ENABLED=0;

    meta = with pkgs.lib; {
      description = "A cloud-native LDAP server backed by a Consul datastore";
      homepage = "https://git.deuxfleurs.fr/Deuxfleurs/bottin";
      license = licenses.gpl3Plus;
      platforms = platforms.linux;
    };
  };
  pkg = pkgs.stdenv.mkDerivation {
    pname = "bottin";
    version = "0.1.0";
    unpackPhase = "true";

    installPhase = ''
      mkdir -p $out/
      cp ${bin}/bin/bottin $out/bottin
    '';
  };
  docker = pkgs.dockerTools.buildImage {
    name = "dxflrs/bottin";
    config = {
      Entrypoint = [ "${pkg}/bottin" ];
      WorkingDir = "/";
    };
  };
}
