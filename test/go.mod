module bottin/integration

go 1.14

require (
	github.com/go-ldap/ldap/v3 v3.3.0
	github.com/sirupsen/logrus v1.4.2
)
