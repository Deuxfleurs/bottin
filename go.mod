module bottin

go 1.13

require (
	github.com/google/uuid v1.1.1
	github.com/hashicorp/consul/api v1.3.0
	github.com/jsimonetti/pwscheme v0.0.0-20220125093853-4d9895f5db73
	github.com/sirupsen/logrus v1.4.2
	golang.org/x/crypto v0.0.0-20200604202706-70a84ac30bf9 // indirect
)
